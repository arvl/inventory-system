#include "updatetabledialog.h"
#include "ui_updatetabledialog.h"

UpdateTableDialog::UpdateTableDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateTableDialog)
{
    ui->setupUi(this);
}

UpdateTableDialog::~UpdateTableDialog()
{
    delete ui;
}

void UpdateTableDialog::setTableName(QString curr_table) {
    ui->ledTableName->setText(curr_table);
}

void UpdateTableDialog::on_btnOKCancel_accepted()
{
    if (!ui->ledTableName->text().isEmpty()) {
        emit updateTable(ui->ledTableName->text());
    }
}
