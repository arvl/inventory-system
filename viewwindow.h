#ifndef VIEWWINDOW_H
#define VIEWWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QtSql>

#include <additemdialog.h>
#include <updateitemdialog.h>

#include <passworddialog.h>

#include <addtabledialog.h>
#include <deletetabledialog.h>
#include <updatetabledialog.h>

namespace Ui {
class ViewWindow;
}

class ViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewWindow(QWidget *parent = nullptr);
    ~ViewWindow();

private slots:

    void loadTablesList();

    void loadTable();

    void loadQuantity();

    void clearQuantity();

    void addItem(QString);

    void deleteItem();

    void updateItem(QString);

    void plusItem();

    void minusItem();

    void addTable(QString);

    void deleteTable();

    void updateTable(QString);

    void authenticate(QString);

    void enableDeleteUpdateBtns();

    void disableDeleteUpdateBtns();

    void on_btnAddItem_clicked();

    void on_btnDeleteItem_clicked();

    void on_actionExit_triggered();

    void on_btnUpdateItem_clicked();

    void on_btnPlusItem_clicked();

    void on_btnMinusItem_clicked();

    void on_actionLogin_triggered();

    void on_actionAddTable_triggered();

    void on_actionDeleteTable_triggered();

    void on_actionUpdateTable_triggered();

signals:
    void setItemName(QString);

    void setTableName(QString);

private:
    Ui::ViewWindow *ui;

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QString dbPath = "classrooms.db";

    bool isAdmin = false;

    AddItemDialog *a;
    UpdateItemDialog *u;

    AddTableDialog *c;
    DeleteTableDialog *d;
    UpdateTableDialog *m;

    PasswordDialog *p;
};

#endif // VIEWWINDOW_H
