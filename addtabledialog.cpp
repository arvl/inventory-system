#include "addtabledialog.h"
#include "ui_addtabledialog.h"

AddTableDialog::AddTableDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddTableDialog)
{
    ui->setupUi(this);
}

AddTableDialog::~AddTableDialog()
{
    delete ui;
}

void AddTableDialog::on_btnOKCancel_accepted()
{
    if (!ui->ledTableName->text().isEmpty()) {
        emit addTable(ui->ledTableName->text());
    }
}
