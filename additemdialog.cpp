#include "additemdialog.h"
#include "ui_additemdialog.h"

AddItemDialog::AddItemDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItemDialog)
{
    ui->setupUi(this);
}

AddItemDialog::~AddItemDialog()
{
    delete ui;
}

void AddItemDialog::on_btnOKCancel_accepted()
{
    if (!ui->ledItemName->text().isEmpty()) {
        emit addItem(ui->ledItemName->text());
    }
}
