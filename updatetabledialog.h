#ifndef UPDATETABLEDIALOG_H
#define UPDATETABLEDIALOG_H

#include <QDialog>

namespace Ui {
class UpdateTableDialog;
}

class UpdateTableDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateTableDialog(QWidget *parent = nullptr);
    ~UpdateTableDialog();

private slots:
    void on_btnOKCancel_accepted();

    void setTableName(QString);

signals:
    void updateTable(QString);

private:
    Ui::UpdateTableDialog *ui;
};

#endif // UPDATETABLEDIALOG_H
