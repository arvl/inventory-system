#include "updateitemdialog.h"
#include "ui_updateitemdialog.h"

UpdateItemDialog::UpdateItemDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateItemDialog)
{
    ui->setupUi(this);
}

UpdateItemDialog::~UpdateItemDialog()
{
    delete ui;
}

void UpdateItemDialog::setItemName(QString item_name) {
    ui->ledItemName->setText(item_name);
}

void UpdateItemDialog::on_btnOKCancel_accepted()
{
    if (!ui->ledItemName->text().isEmpty()) {
        emit updateItem(ui->ledItemName->text());
    }
}
