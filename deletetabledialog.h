#ifndef DELETETABLEDIALOG_H
#define DELETETABLEDIALOG_H

#include <QDialog>

namespace Ui {
class DeleteTableDialog;
}

class DeleteTableDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DeleteTableDialog(QWidget *parent = nullptr);
    ~DeleteTableDialog();

private slots:
    void on_buttonBox_accepted();

signals:
    void deleteTable();

private:
    Ui::DeleteTableDialog *ui;
};

#endif // DELETETABLEDIALOG_H
