#include "deletetabledialog.h"
#include "ui_deletetabledialog.h"

DeleteTableDialog::DeleteTableDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeleteTableDialog)
{
    ui->setupUi(this);
}

DeleteTableDialog::~DeleteTableDialog()
{
    delete ui;
}

void DeleteTableDialog::on_buttonBox_accepted()
{
    emit deleteTable();
}
