#include "viewwindow.h"
#include "ui_viewwindow.h"

ViewWindow::ViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewWindow)
{
    ui->setupUi(this);
    loadTablesList();
    loadTable();

    connect(ui->cbxTable, SIGNAL(currentTextChanged(QString)), this, SLOT(loadTable()));
    connect(ui->cbxTable, SIGNAL(currentTextChanged(QString)), this, SLOT(clearQuantity()));
    connect(ui->cbxTable, SIGNAL(currentTextChanged(QString)), this, SLOT(disableDeleteUpdateBtns()));


    connect(ui->lsvwTable, SIGNAL(clicked(QModelIndex)), this, SLOT(loadQuantity()));
    connect(ui->lsvwTable, SIGNAL(clicked(QModelIndex)), this, SLOT(enableDeleteUpdateBtns()));
}

ViewWindow::~ViewWindow()
{
    delete ui;
}

void ViewWindow::loadTablesList() {
    db.setDatabaseName(dbPath);
    db.open();

    QSqlQueryModel *qry_model = new QSqlQueryModel();
    qry_model->setQuery(db.exec("SELECT distinct tbl_name FROM sqlite_master;"));
    ui->cbxTable->setModel(qry_model);

    db.close();
}

void ViewWindow::loadTable() {
    db.open();

    QSqlQueryModel *ls_model = new QSqlQueryModel();
    ls_model->setQuery(db.exec("SELECT item FROM '" + ui->cbxTable->currentText() + "'"));
    ui->lsvwTable->setModel(ls_model);

    db.close();
}

void ViewWindow::loadQuantity() {
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString index_text = index.data(Qt::DisplayRole).toString();
    QString curr_table = ui->cbxTable->currentText();
    QSqlQuery query;

    db.open();
    query.exec("SELECT Quantity FROM '" + curr_table + "' WHERE Item = '" + index_text + "'");
    query.next();
    QString curr_quantity = query.value(0).toString();
    db.close();

    ui->ledQuantity->setText(curr_quantity);
}

void ViewWindow::clearQuantity() {
    ui->ledQuantity->clear();
}

void ViewWindow::enableDeleteUpdateBtns() {
    if (isAdmin) {
        ui->btnDeleteItem->setEnabled(true);
        ui->btnUpdateItem->setEnabled(true);
        ui->btnAddItem->setEnabled(true);
        ui->btnPlusItem->setEnabled(true);
        ui->btnMinusItem->setEnabled(true);
    }
}


void ViewWindow::disableDeleteUpdateBtns() {
    if (isAdmin) {
        ui->btnDeleteItem->setEnabled(false);
        ui->btnUpdateItem->setEnabled(false);
        ui->btnPlusItem->setEnabled(false);
        ui->btnMinusItem->setEnabled(false);
    }
}

void ViewWindow::authenticate(QString password) {
    if (password == "idk") {
        ui->btnAddItem->setEnabled(true);
        ui->actionAddTable->setEnabled(true);
        ui->actionDeleteTable->setEnabled(true);
        ui->actionUpdateTable->setEnabled(true);
        isAdmin = true;
    }
}

void ViewWindow::addItem(QString item_name) {
    QString curr_table = ui->cbxTable->currentText();
    qDebug() << "SQL query: INSERT INTO '" + curr_table + "' VALUES ('" + item_name + "', 0)";

    db.open();
    db.exec("INSERT INTO '" + ui->cbxTable->currentText() + "' VALUES ('" + item_name + "', 0)");
    db.close();

    loadTable();
}

void ViewWindow::deleteItem() {
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString index_text = index.data(Qt::DisplayRole).toString();
    QString curr_table = ui->cbxTable->currentText();
    qDebug() << "SQL query: DELETE FROM '" + curr_table + "' WHERE Item = '" + index_text + "'";

    db.open();
    db.exec("DELETE FROM '" + curr_table + "' WHERE Item = '" + index_text + "'");
    db.close();

    loadTable();
}

void ViewWindow::updateItem(QString item_name) {
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString index_text = index.data(Qt::DisplayRole).toString();
    QString curr_table = ui->cbxTable->currentText();
    qDebug() << "SQL query: UPDATE '" + curr_table + "' SET Item = '" + item_name + "' WHERE Item = '" + index_text + "'";

    db.open();
    db.exec("UPDATE '" + curr_table + "' SET Item = '" + item_name + "' WHERE Item = '" + index_text + "'");
    db.close();

    loadTable();
}

void ViewWindow::plusItem() {
    QString curr_table = ui->cbxTable->currentText();
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString index_text = index.data(Qt::DisplayRole).toString();
    int curr_quantity = ui->ledQuantity->text().toInt();
    curr_quantity += 1;
    if (curr_quantity >= 0) {
        QString strcurr_quantity = QString::number(curr_quantity);
        qDebug() << "SQL query: UPDATE '" + curr_table + "' SET Quantity = " + strcurr_quantity + " WHERE Item = '" + index_text + "'";
        db.open();
        db.exec("UPDATE '" + curr_table + "' SET Quantity = " + strcurr_quantity + " WHERE Item = '" + index_text + "'");
        db.close();
        loadQuantity();
    }
}

void ViewWindow::minusItem() {
    QString curr_table = ui->cbxTable->currentText();
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString index_text = index.data(Qt::DisplayRole).toString();
    int curr_quantity = ui->ledQuantity->text().toInt();
    curr_quantity -= 1;
    if (curr_quantity >= 0) {
        QString strcurr_quantity = QString::number(curr_quantity);
        qDebug() << "SQL query: UPDATE '" + curr_table + "' SET Quantity = " + strcurr_quantity + " WHERE Item = '" + index_text + "'";
        db.open();
        db.exec("UPDATE '" + curr_table + "' SET Quantity = " + strcurr_quantity + " WHERE Item = '" + index_text + "'");
        db.close();
        loadQuantity();
    }
}

void ViewWindow::addTable(QString table_name) {
    qDebug() << "SQL query: CREATE TABLE '" + table_name + "' (Item VARCHAR(255) PRIMARY KEY, Quantity INTEGER)";

    db.open();
    db.exec("CREATE TABLE '" + table_name + "' (Item VARCHAR(255) PRIMARY KEY, Quantity INTEGER)");
    db.close();

    loadTablesList();
}

void ViewWindow::deleteTable() {
    QString curr_table = ui->cbxTable->currentText();
    qDebug() << "SQL query: DROP TABLE '" + curr_table + "'";

    db.open();
    db.exec("DROP TABLE '" + curr_table + "'");
    db.close();

    loadTablesList();
}

void ViewWindow::updateTable(QString table_name) {
    QString curr_table = ui->cbxTable->currentText();
    qDebug() << "SQL query: ALTER TABLE '" + curr_table + "' RENAME TO '" + table_name + "'";

    db.open();
    db.exec("ALTER TABLE '" + curr_table + "' RENAME TO '" + table_name + "'");
    db.close();

    loadTablesList();
}

void ViewWindow::on_btnAddItem_clicked()
{
    a = new AddItemDialog(this);
    a->setModal(true);
    a->show();
    connect(a, SIGNAL(addItem(QString)), this, SLOT(addItem(QString)));
}

void ViewWindow::on_btnDeleteItem_clicked()
{
    deleteItem();
}

void ViewWindow::on_btnUpdateItem_clicked()
{
    u = new UpdateItemDialog(this);
    u->setModal(true);
    u->show();
    QModelIndex index = ui->lsvwTable->currentIndex();
    QString index_text = index.data(Qt::DisplayRole).toString();
    connect(this, SIGNAL(setItemName(QString)), u, SLOT(setItemName(QString)));
    emit setItemName(index_text);
    connect(u, SIGNAL(updateItem(QString)), this, SLOT(updateItem(QString)));
}

void ViewWindow::on_btnPlusItem_clicked()
{
    plusItem();
}

void ViewWindow::on_btnMinusItem_clicked()
{
    minusItem();
}

void ViewWindow::on_actionAddTable_triggered()
{
    c = new AddTableDialog(this);
    c->setModal(true);
    c->show();
    connect(c, SIGNAL(addTable(QString)), this, SLOT(addTable(QString)));
}

void ViewWindow::on_actionDeleteTable_triggered()
{
    d = new DeleteTableDialog(this);
    d->setModal(true);
    d->show();
    connect(d, SIGNAL(deleteTable()), this, SLOT(deleteTable()));
}

void ViewWindow::on_actionUpdateTable_triggered()
{
    m = new UpdateTableDialog(this);
    m->setModal(true);
    m->show();
    QString curr_table = ui->cbxTable->currentText();
    connect(this, SIGNAL(setTableName(QString)), m, SLOT(setTableName(QString)));
    emit setTableName(curr_table);
    connect(m, SIGNAL(updateTable(QString)), this, SLOT(updateTable(QString)));
}

void ViewWindow::on_actionLogin_triggered()
{
    p = new PasswordDialog(this);
    p->setModal(true);
    p->show();
    connect(p, SIGNAL(authenticate(QString)), this, SLOT(authenticate(QString)));
}

void ViewWindow::on_actionExit_triggered()
{
    close();
}
